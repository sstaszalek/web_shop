# Online shop #

Web shop application. It is made for learning purposes, so it may contain redundant code 
as well as unused fragments of codes as examples used during studying.

### Funcionalities:
- CRUD operations:
- Creating new products and users
- Reading users, shopping cart and history, 
- Updating products, ratings, comments etc.
- Deleting products, comments etc.



### Dependencies 
- Junit - https://mvnrepository.com/artifact/junit/junit
- Hibernate's core ORM functionality - https://mvnrepository.com/artifact/org.hibernate/hibernate-core
- JDBC driver for MySQL - https://mvnrepository.com/artifact/mysql/mysql-connector-java
- SLF4J LOG4J-12 Binding - https://mvnrepository.com/artifact/org.slf4j/slf4j-log4j12
- JSTL - https://mvnrepository.com/artifact/javax.servlet.jsp.jstl/jstl
- Java Servlet Api - https://mvnrepository.com/artifact/javax.servlet/javax.servlet-api
- Project Lombok - Automatic Resource Management for Java - https://mvnrepository.com/artifact/org.projectlombok/lombok

> Project implemented as a part of my Java course in Software Development Academy