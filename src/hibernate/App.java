package hibernate;//package hibernate;
//
//import hibernate.shop.*;
//
//
//import java.math.BigDecimal;
//import java.util.*;
//
//
///**
// * Created by Lukasz on 27.09.2017.
// */
//public class App {
//
//    public static void main(String[] args) throws Exception {
//
//        Product merc = new Product("Mercedes S", ProductType.CAR,
//                new Price(new BigDecimal(500000), new BigDecimal(600000)));
//
//        Product mercA = new Product("Mercedes A", ProductType.CAR,
//                new Price(new BigDecimal(100000), new BigDecimal(120000)));
//
//        Product smallCar = new Product("red car", ProductType.TOY,
//                new Price(new BigDecimal(30), new BigDecimal(34)));
//
//
//        ProductRepository.saveProduct(merc);
//        ProductRepository.saveProduct(mercA);
//        ProductRepository.saveProduct(smallCar);
//
//        Optional<Product> product1 = ProductRepository.findOneById(1L);
//        Optional<Product>  product2 = ProductRepository.findOneById(2L);
//        Optional<Product>  product3 = ProductRepository.findOneById(3L);
//
//        System.out.println(product1.map(p -> p.getName()).orElse(""));
//        System.out.println(product2.map(p -> p.getName()).orElse(""));
//        System.out.println(product3.map(p -> p.getName()).orElse(""));
//
//
//        ProductRepository.findAll().forEach(p -> System.out.println("findAll() "+p.getName()));
//
//
//        ProductRepository.findAllByProductType(ProductType.CAR)
//                .forEach(p -> System.out.println("type car: "+p.getName()));
//
//
//        ProductRepository.findAllByProductType(ProductType.TOY)
//                .forEach(p -> System.out.println("type toy: "+p.getName()));
//
//        Long carCount = ProductRepository.countByProductType(ProductType.CAR);
//        System.out.println("car in db " + carCount);
//
//        ProductRepository.findAllWithPriceLess(
//                new BigDecimal(40))
//                .forEach(p -> System.out.println("product with price less: "+p.getName() +" "+ p.getPrice().getVatPrice()));
//
//        ProductRepository.findAllByNameLike("merc")
//                .forEach(p -> System.out.println("find All by merc: "+p.getName()));
//
//
//        Optional<Product>  toyOptional = ProductRepository.findOneById(3L);
//
//        if(toyOptional.isPresent()){
//            Product toy = toyOptional.get();
//            toy.getPrice().setGrossPrice(toy.getPrice().getGrossPrice().add(BigDecimal.ONE));
//            toy.getPrice().setNettoPrice(toy.getPrice().getNettoPrice().add(BigDecimal.ONE));
//
//            ProductRepository.saveProduct(toy);
//
//        }
//
//        ProductRepository.deleteById(2L);
//
//
//        OrderDetail orderDetail = OrderDetail.builder().
//                amount(BigDecimal.ONE)
//                .product(product1.get())
//                .price(product1.get().getPrice())
//                .build();
//
//
//        OrderDetail orderDetail2 = OrderDetail.builder().
//                amount(BigDecimal.TEN)
//                .product(product3.get())
//                .price(product3.get().getPrice())
//                .build();
//
//        OrderDetail orderDetail3 = OrderDetail.builder().
//                amount(BigDecimal.TEN)
//                .product(product1.get())
//                .price(product1.get().getPrice())
//                .build();
//
//        Order order = Order.builder()
//                .userEmail("test@sda.pl")
//                .totalGross(new BigDecimal(13))
//                .totalNetto(new BigDecimal(12))
//                .orderDetailList(new HashSet<>())
//                .build();
//
//        order.addOrderDetail(orderDetail);
//        order.addOrderDetail(orderDetail2);
//        order.addOrderDetail(orderDetail3);
//
//        OrderRepository.saveOrder(order);
//
//        List<Order> all = OrderRepository.findAll();
//
//        System.out.println("d");
//
//        ProductRepository.findAllNative().
//                forEach(p -> System.out.println("find Product All from native: "+p.getName()));
//
//
//        OrderRepository.findAllOrderWithProduct(1L).
//                forEach(p -> System.out.println("find order with product: "+ p.getId()));
//
//        List<Order> allOrderWithProduct = OrderRepository.findAll();
//
//        for(Order o : allOrderWithProduct){
//
//            o.getOrderDetailList().stream().forEach(od ->
//                    System.out.println("zamowienie o id"+o.getId()+" "+od.getProduct().getName()));
//        }
//
//
//
//        Set<Order> orderSet = new HashSet<>();
//        orderSet.add(order);
//
//        OrderComplaint orderComplaint = OrderComplaint.builder()
//                .complaintStatus(ComplaintStatus.PENDING)
//                .message("New complaint from John")
//                .orderSet(orderSet).build();
//
//        OrderComplaintRepository.saveOrderComplaint(orderComplaint);
//
//
//        ProductRepository.findAllByNameLikeWithCriteria("merc")
//                .forEach(p -> System.out.println("find All by merc: "+p.getName()));
//
//
//
//        ProductRepository.findAllToyOrCarWithNameLikePredicate("merc")
//                .forEach(p -> System.out.println("findAllToyOrCarWithPriceLessThan: "+p.getName()));
//
//
//        CartDetail cartDetail = CartDetail.builder().
//                amount(BigDecimal.ONE)
//                .product(product1.get())
//                .price(product1.get().getPrice())
//                .build();
//
//        Cart cart = Cart.builder().
//                email("kowalski@wp.pl").
//                cartDetailSet(new HashSet<>())
//                .build();
//
//        cart.addCartDetail(cartDetail);
//
//        CartRepository.saveCart(cart);
//
//        Optional<Cart> oneById = CartRepository.findOneById(cart.getId());
//        if(oneById.isPresent()){
//            System.out.println("cena koszyka "+ oneById.get().getPrice().getGrossPrice());
//        }
//    }
//}
public class App {
    public static void main(String[] args) {

    }
}
