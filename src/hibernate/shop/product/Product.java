package hibernate.shop.product;

import hibernate.shop.Price;
import hibernate.shop.order.OrderDetail;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;



@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"orderDetailSet", "productRatingSet"})
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Column(name = "data_dodania")
    private LocalDate date;

    @Enumerated
    private ProductType productType;

    @Embedded
    private Price price;
    private String description;

    @Lob
    private byte[] image;

    //jeden produkt moze byc na wielu pozycji zamowieniach
    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
    Set<OrderDetail> orderDetailSet;

    @OneToMany(mappedBy = "product")
    Set<ProductRating> productRatingSet;

}



