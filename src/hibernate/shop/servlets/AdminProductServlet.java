package hibernate.shop.servlets;

import hibernate.shop.*;
import hibernate.shop.product.Product;
import hibernate.shop.product.ProductRepository;
import hibernate.shop.product.ProductType;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;

@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
        maxFileSize=1024*1024*10,      // 10MB
        maxRequestSize=1024*1024*50)   // 50MB
@WebServlet(name = "AdminProductServlet", urlPatterns = "/editOrAddProduct")
public class AdminProductServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String productType = request.getParameter("productType");
        BigDecimal nettoPrice = ProjectHelper.parseStringToBigDecimal(request.getParameter("nettoPrice"));
        BigDecimal grossPrice = ProjectHelper.parseStringToBigDecimal(request.getParameter("grossPrice"));

        Product product = new Product();
        product.setDate(LocalDate.now());
        product.setDescription(description);
        product.setName(name);
        Price price = new Price();
        price.setGrossPrice(grossPrice);
        price.setNettoPrice(nettoPrice);
        product.setPrice(price);
        product.setProductType(ProductType.valueOf(productType));

        InputStream input = request.getPart("image").getInputStream();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[10240];
        for (int length = 0; (length = input.read(buffer)) > 0; ) output.write(buffer, 0, length);

        product.setImage(output.toByteArray());

        ProductRepository.saveProduct(product);
    }
}
