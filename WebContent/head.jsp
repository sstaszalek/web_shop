<%@ page import="hibernate.shop.user.UserSessionHelper" %>
<%@ page import="hibernate.shop.user.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    User userFromCookie = UserSessionHelper.getUserFromCookie(request.getCookies());

    if(userFromCookie != null){
        pageContext.setAttribute("user", userFromCookie);
    }

    Object i = session.getAttribute("i");
    if(i == null) {
        session.setAttribute("i", 1);
    } else {
        session.setAttribute("i", (int)i +1);
    }
    pageContext.setAttribute("i", session.getAttribute("i"));
%>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/index.jsp">Super Jeff</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a class="nav-link" href="#">Licznik: ${i}</a>
                </li>

                <c:if test="${user == null}" >
                    <li class="nav-item">
                        <a class="nav-link" href="/login.jsp">Login</a>
                    </li>
                </c:if>
                <c:if test="${user != null}" >
                    <li class="nav-item">
                        <a class="nav-link" href="/jsp/cart.jsp">Koszyk</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/orderHistory.jsp">Orders</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" >${user.firstName} ${user.lastName}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/logout">Logout</a>
                    </li>
                </c:if>
            </ul>
        </div>
    </div>
</nav>